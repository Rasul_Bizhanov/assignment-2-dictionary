%include "colon.inc"
%include "dict.inc"
%include "lib.inc"
section .rodata

err_message: db 'Cannot find key', 0
ov_message: db 'Out of buffer size', 0
%define LENGTH 256

section .data

%include "words.inc"

section .bss
str: resb LENGTH
section .text
global _start

_start:

	mov rsi, LENGTH
	mov rdi, str
	call read_line
	cmp rax, 0
	je .out_of_size
	mov rdi, last
	mov rsi, rax
	call find_word
	cmp rax, 0
	je .error
	push rax
	call string_length
	pop rdi
	lea rdi, [rdi + rax + 1]
	call print_string
	call exit

.out_of_size:
	mov rdi, ov_message
	jmp .end
.error:
	mov rdi, err_message
.end:
	call print_err
	call exit
