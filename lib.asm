%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define STDOUT 1
%define NEWLINE_CHAR `\n`
%define SPACE_CHAR ' '
%define TAB_CHAR `\t`
%define ZERO '0'
%define NINE '9'
%define PLUS '+'
%define MINUS '-'

section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; ��������� ��� �������� � ��������� ������� �������
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; ��������� ��������� �� ����-��������������� ������, ���������� � �����
string_length:
    xor rax, rax
  .begin:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .begin
  .end: 
    ret


; ��������� ��������� �� ����-��������������� ������, ������� � � stdout
print_string:
   push rdi
   call string_length
   pop rsi
   mov rdx, rax
   mov rax, WRITE_SYSCALL
   mov rdi, STDOUT
   syscall
   ret

; ��������� ������ (������� ������ � ����� 0xA)
print_newline:
    mov rdi, 0xA

; ��������� ��� ������� � ������� ��� � stdout
print_char:
   push rdi
   mov rax, WRITE_SYSCALL
   mov rsi, rsp
   mov rdi, STDOUT
   mov rdx, 1
   syscall
   pop rdi
   ret

; ������� ����������� 8-�������� ����� � ���������� ������� 
; �����: �������� ����� � ����� � ������� ��� ���������� �������
; �� �������� ��������� ����� � �� ASCII ����.
print_uint:
	mov rax, rdi
    dec rsp
    mov byte[rsp], 0x0
    mov rsi, 1
    mov rcx, 10
.begin:
    xor rdx, rdx
    div rcx
    add rdx, ZERO
    dec rsp
    mov [rsp], dl
    inc rsi
    test rax, rax
    jz .end
    jmp .begin
.end:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    ret


; ������� �������� 8-�������� ����� � ���������� ������� 
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov rdi, MINUS
	call print_char
	pop rdi
	neg rdi
 .print:
	call print_uint
    ret


; ��������� ��� ��������� �� ����-��������������� ������, ���������� 1 ���� ��� �����, 0 �����
string_equals:
    xor rcx, rcx
	xor rax, rax
.begin:
	mov al, byte[rdi + rcx]
	mov ah, byte[rsi + rcx]
	cmp al, ah
	jne .no
	test al, al
	je .yes
	inc rcx
	jmp .begin
.yes:
	mov rax, 1
	ret
.no:
	xor rax, rax
	ret


; ������ ���� ������ �� stdin � ���������� ���. ���������� 0 ���� ��������� ����� ������
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp
    mov rdx, 1
    xor rdi, rdi
    syscall
    pop rax
    cmp rax, 0 
    je  .end_of_file
    ret
.end_of_file:
    mov rax, 0 
    ret


; ���������: ����� ������ ������, ������ ������
; ������ � ����� ����� �� stdin, ��������� ���������� ������� � ������, .
; ���������� ������� ��� ������ 0x20, ��������� 0x9 � ������� ������ 0xA.
; ��������������� � ���������� 0 ���� ����� ������� ������� ��� ������
; ��� ������ ���������� ����� ������ � rax, ����� ����� � rdx.
; ��� ������� ���������� 0 � rax
; ��� ������� ������ ���������� � ����� ����-����������

read_word:
	push r12
	push r13
	xor rcx, rcx
	mov r12, rdi
	dec rsi
	mov r13, rsi
.begin_space:
	push rcx
	call read_char
	pop rcx
	cmp rax, SPACE_CHAR
	je .begin_space
	cmp rax, TAB_CHAR
	je .begin_space
	cmp rax, NEWLINE_CHAR
	je .begin_space

.begin:
	test rax, rax 
	je .add_and_ret
	cmp rax, SPACE_CHAR
	je .add_and_ret
	cmp rax, TAB_CHAR
	je .add_and_ret
	cmp rax, NEWLINE_CHAR
	je .add_and_ret
	cmp r13, rcx 
	je .stop
	mov byte[r12 + rcx], al 
	inc rcx 
	push rcx
	call read_char
	pop rcx
	jmp .begin

.add_and_ret:
	mov byte[r12 + rcx], 0
	mov rax, r12
	mov rdx, rcx
	jmp .return
.stop:
	xor rax, rax
.return:
	pop r13
	pop r12
	ret

 

; ��������� ��������� �� ������, ��������
; ��������� �� � ������ ����������� �����.
; ���������� � rax: �����, rdx : ��� ����� � ��������
; rdx = 0 ���� ����� ��������� �� �������
parse_uint:
    push rbx
    xor rdx, rdx
    xor rax, rax
.begin:
	mov bl, byte[rdi+rdx]
	test bl, bl
	jz .end
	cmp bl, ZERO
	jl .error
	cmp bl, NINE
	jg .error
	sub rbx, ZERO
	imul rax, 10
	add rax, rbx
	inc rdx
	jmp .begin
.error:
	test rax, rax
	jnz .end
	xor rdx, rdx
.end:
	pop rbx
	ret




; ��������� ��������� �� ������, ��������
; ��������� �� � ������ �������� �����.
; ���� ���� ����, ������� ����� ��� � ������ �� ���������.
; ���������� � rax: �����, rdx : ��� ����� � �������� (������� ����, ���� �� ���) 
; rdx = 0 ���� ����� ��������� �� �������
parse_int:
   cmp byte [rdi], PLUS
   je  parse_uint 
   cmp byte [rdi], MINUS
   jne parse_uint
   inc rdi
   call parse_uint
   inc rdx
   neg rax
   ret


; ��������� ��������� �� ������, ��������� �� ����� � ����� ������
; �������� ������ � �����
; ���������� ����� ������ ���� ��� ��������� � �����, ����� 0
string_copy:
   xor rax, rax
   push rbx
.begin:
   cmp rax, rdx
   je .full
   mov bl, byte[rdi + rax]
   mov byte[rsi + rax], bl
   test rbx, rbx
   je .end
   inc rax
   jmp .begin
.full:
   xor rax, rax
   jmp .end
.end:
   pop rbx
   ret

read_line:
	push r12
	push r13
	push r14
	push rsi
	push rdi
.begin:
	call read_char
	cmp al, TAB_CHAR
	je .begin
	cmp al, NEWLINE_CHAR
	je .begin
	cmp al, SPACE_CHAR
	je .begin
	pop r12
	pop r13
	xor r14, r14
.begin_word:
	cmp al, NEWLINE_CHAR
	je .terminate
	test rax, rax
	jz .terminate
	cmp r14, r13
	jg .error
	mov [r12 + r14], al
	inc r14
	call read_char
	jmp .begin_word
.error:
	xor rax, rax
	xor rdx, rdx
	jmp .restore
.terminate:
	mov byte[r12 + r14], 0
	mov rax, r12
	mov rdx, r14
.restore:
	pop r14
	pop r13
	pop r12
	ret
 
print_err:
	mov rsi, 2
	push rdi
	push rsi
    call string_length
	pop rdi
	pop rsi
    mov rdx, rax
    mov rax, 1
    syscall