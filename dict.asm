%include "lib.inc"

section .data
section .text

global find_word

find_word:
	push rbx
.begin:
	mov rbx, rdi
	lea rdi, [rdi + 8]
	call string_equals
	cmp rax, 1
	je .end
	mov rdi, qword[rbx]
	test rdi, rsi
	je .error
	jmp .begin
.error:
	xor rdi, rdi
.end:
	mov rax, rdi
	pop rbx
	ret